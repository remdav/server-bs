import Router from "koa-router"

export default () => {
    const route = new Router()

    const { connection, users, meals, animations } = require("application")
    
    route.use("/connection", connection.routes(), connection.allowedMethods())
    route.use("/animations", animations.routes(), animations.allowedMethods())
    route.use("/meals", meals.routes(), meals.allowedMethods())
    route.use("/users", users.routes(), users.allowedMethods())

    route.use(ctx => {
        ctx.status = 404
    })

    return route
}


