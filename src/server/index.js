import responseTime from "koa-response-time"
import config from "configs/server.json"
import bodyParser from "koa-bodyparser"
import bootstrap from "./bootstrap"
import Sequelize from "sequelize"
import helmet from "koa-helmet"
import serve from "koa-static"
import mount from "koa-mount"
import eTag from "koa-etag"
import Koa from "koa"

const app = new Koa()
export default app

app.use(mount("/images", serve(`${__dirname}/../../public/images`)))
app.use(responseTime())
app.use(helmet())
app.use(eTag())

app.use(bodyParser())

app.listen(config.global.port, error => {
    if (error) {
        throw new Error("Error pending start server", error)
    }
})

const { database, user, password, host } = config.global.db

const sequelize = new Sequelize(database, user, password, {
    host: host,
    dialect: "mariadb",
    dialectOptions: {
        timezone: 'Etc/GMT+1',
    },
    logging: false
})

app.context.sequelize = sequelize

sequelize.sync({})

const mainRouter = bootstrap()
app.use(mainRouter.routes())
app.use(mainRouter.allowedMethods())
