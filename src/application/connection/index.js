import Router from "koa-router"

import { login, jwtConnection } from "./action"

const route = new Router()

route.post("/external", jwtConnection)
route.post("/login", login)

export default route