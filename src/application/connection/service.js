import { users, externalUser } from "application/users/models"
import { createExternalUser } from "application/users/service"

export async function create(info) {
    let newExternal

    try {
        newExternal = await createExternalUser({ idExternal: info.id, role: "user" })
    } catch(e) {
        console.log("Error pending create new external user", e)
        return
    }

    return newExternal
}

export function searchUser(username) {
    return users.findOne({ where: { username } })
}

export function isExternalExist(idExternal) {
    return externalUser.findOne({ where: { idExternal } })
}
