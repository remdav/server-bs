import { configureJwt, keyRandom } from "utils"
import bcrypt from "bcrypt"

import { create, searchUser, isExternalExist } from "./service"

export async function login(ctx) {
    const { name, password } = ctx.request.body

    if (name === "" && password === "") {
        ctx.status = 400
        ctx.body = { text: "INFO_MISSING" }
        return
    }

    let findUser

    try {
        findUser = await searchUser(name)
    } catch (e) {
        console.log("Error pending find user for login", e)
        ctx.status = 500
        return
    }

    if (findUser === null || undefined || "") {
        console.log("Error User no exist")
        ctx.status = 404
        ctx.body = { text: "USER_NO_EXIST" }
        return
    } else {
        let compare = await (bcrypt.compare(password, findUser.password))
        const { firstname, name, role } = findUser.dataValues
        const fullName = `${firstname} ${name}`

        if (compare) {
            ctx.body = {
                items: {
                    name: fullName,
                    role,
                    tokenKey: await configureJwt({ name: fullName, role }),
                }
            }
            return
        }

        console.log("Error Login or passowrd")
        ctx.status = 400
        ctx.body = { text: "ERROR_ID" }
        return
    }

}

export async function jwtConnection(ctx) {
    const key = keyRandom()
    const { info } = ctx.request.body
    const user = await isExternalExist(info.id)
    let newUser

    if(!user) {
        try{
            newUser = await create(info)
        } catch (e) {
            ctx.status = 500
            console.log("Error pending create external user")
            return
        }
    }

    ctx.body = {
        items: {
            tokenKey: await configureJwt({name: info.name, role: !!user ? user.role : newUser.dataValues.role, external: true}, key),
            key: key
        }
    }
    return
}