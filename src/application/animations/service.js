import { animations } from "./models"

export function getAllAnimation() {
  return animations.findAll()
}

export async function createOneAnimation(info) {
  let newAnimation

  try {
    newAnimation = await animations.build(info).save()
  } catch (e) {
    console.log("Error pending insert new animation inside db", e)
    return
  }

  return newAnimation.dataValues
}

export async function updateAnimation(id, info) {

  try {
    await animations.update(info, { where: { id } })
  } catch (e) {
    console.log("Error pending save update", e)
    return
  }

  return animations.findByPk(id)
}

export async function removeAnimation(id) {
  return await animations.destroy({ where: { id } })
}