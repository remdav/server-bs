import { DataTypes } from "sequelize"
import app from "server"

const Animations = app.context.sequelize.define("animations", {
  title: {
    type: DataTypes.STRING(50),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  theme: {
    type: DataTypes.STRING(50),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  describe: {
    type: DataTypes.TEXT,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  speaker: {
    type: DataTypes.STRING(100),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  start: {
    type: DataTypes.DATE,
    defaultValue: app.context.sequelize.NOW,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  end: {
    type: DataTypes.DATE,
    defaultValue: app.context.sequelize.NOW,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  picture: {
    type: DataTypes.STRING(100),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: "users",
      key: "id"
    }
  }
}, {
  indexes: [
    {
      fields: ["title"]
    },
    {
      fields: ["theme"]
    },
    {
      fields: ["speaker"]
    },
  ]
})

Animations.associate = function(models) {
  Animations.belongTo(models.Users, { foreingKey: "users_id", as: "users" })
}

export default Animations