import { getAllAnimation, createOneAnimation, updateAnimation, removeAnimation } from "./service"

export async function getAll(ctx) {
  let allAnimation

  try {
    allAnimation = await getAllAnimation()
  } catch (e) {
    console.log("Error return all animation", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: allAnimation
  }
}

export async function create(ctx) {
  const { title, theme, describe, speaker, start, end, picture, userId } = ctx.request.body

  if(!title || !theme || !describe || !speaker || !start || !end || !picture || !userId) {
    ctx.body = { text: "BAD_PARAMETER" }
    ctx.status = 400
    return
  }

  let newAnimation

  try {
    newAnimation = await createOneAnimation({ title, theme, describe, speaker, start, end, picture, userId })
  } catch (e) {
    console.log("Error create animation", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: newAnimation
  }
}

export async function update(ctx) {
  const { title, theme, describe, speaker, start, end, picture, userId } = ctx.request.body
  const { id } = ctx.params

  if(!title || !theme || !describe || !speaker || !start || !end || !picture || !userId || !id) {
    ctx.body = { text: "BAD_PARAMETER" }
    ctx.status = 400
    return
  }
  let updateAnim

  try {
    updateAnim = await updateAnimation(id, { title, theme, describe, speaker, start, end, picture, userId })
  } catch (e) {
    console.log("Error update animation", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: updateAnim
  }
}

export async function remove(ctx) {
  const { id } = ctx.params

  try {
    await removeAnimation(id)
  } catch (e) {
    console.log("Error pending remove animation", e)
    ctx.status = 500
    return
  }

  ctx.status = 200
}