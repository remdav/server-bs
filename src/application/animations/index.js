import Router from "koa-router"

import { getAll, create, update, remove } from "./action"

const route = new Router()

route.put("/:id", update)
route.delete("/:id", remove)
route.get("/", getAll)
route.post("/", create)

export default route