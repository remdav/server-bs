import { getMeal, allCategories, createcategorie, updateCategorie, deleteCat, createNewMeal, updateOneMeal, deleteOneMeal, getAllMeals,AllMealsByCategories } from "./service"

export async function allMeals(ctx) {
  const { title } = ctx.query
  let allMeals

  try {
    allMeals = await getAllMeals(title)
  } catch(e) {
    ctx.status = 500
    console.log("Error pending get all meals", e)
    return
  }

  ctx.body = {
    items: allMeals
  }
}

export async function oneMeals(ctx) {
  const { id } = ctx.params
  let meal

  try {
    meal = await getMeal(id)
  } catch(e) {
    ctx.status = 500
    console.log("Error pending get all meals", e)
    return
  }

  ctx.body = {
    items: meal
  }
}

export async function mealsByCat(ctx) {
  const { id } = ctx.params
  const { title } = ctx.query

  let mealsByCategories

  try {
    mealsByCategories = await AllMealsByCategories(id, title)
  } catch (e) {
    ctx.status = 500
    console.log("Error pending ge all meals")
    return
  }

  ctx.body = {
    items: mealsByCategories
  }
}

export async function createMeal(ctx) {
  const { title, describe, stock, price, picture, categorieId } = ctx.request.body

  if(!title || !describe || !stock || !price || !picture, !categorieId) {
    ctx.status = 400
    ctx.body = { text: "BAD_PARAMETER" }
    return
  }
  let newMeal
  
  try {
    newMeal = await createNewMeal({ title, describe, stock, price, picture, categorieId })
  } catch (e) {
    console.log("Error pending create new meal", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: newMeal
  }
}

export async function updateMeal(ctx) {
  const { title, describe, stock, price, picture, categorieId } = ctx.request.body
  const { id } = ctx.params
  
  if(!title || !describe || !stock || !price || !picture || !categorieId || !id) {
    ctx.status = 400
    ctx.body = { text: "BAD_PARAMETER" }
    return
  }
  let update

  try {
    update = await updateOneMeal(id, { title, describe, stock, price, picture, categorieId })
  } catch (e) {
    console.log("Error update meal inside action", e)
    ctx.status = 500
    return
  }

  ctx.body = {
    items: update
  }
}

export async function deleteMeal(ctx) {
  const { id } = ctx.params

  try {
    await deleteOneMeal(id)
  } catch (e) {
    console.log("Error delete meal", e)
    ctx.status = 500
    return
  }

  ctx.status = 200
}

export async function allCat(ctx) {
  let allCat

  try {
    allCat = await allCategories()
  } catch (e) {
    ctx.status = 500
    console.log("Error pending get all categorie meal", e)
    return
  }

  ctx.body = {
    items: allCat
  }
}

export async function createCat(ctx) {
  const { title, picture } = ctx.request.body

  if(!title || !picture) {
    ctx.request = 400
    ctx.body = { text: "BAD_PARAMETER" }
    return
  }

  let newCat

  try {
    newCat = await createcategorie(title, picture)
  } catch (e) {
    ctx.status = 500
    console.log("Error action pending create new categorie meal", e)
    return
  }

  ctx.body = {
    items: newCat
  }
}

export async function updateCat(ctx) {
  const { title, picture } = ctx.request.body
  const { id } = ctx.params

  if(!title || !picture) {
    ctx.request = 400
    ctx.body = { text: "BAD_PARAMETER" }
    return
  }

  let updateCat

  try {
    updateCat = await updateCategorie(id, title, picture)
  } catch (e) {
    ctx.status = 500
    console.log("Error action pending update categorie meal", e)
    return
  }

  ctx.body = {
    items: updateCat
  }
}

export async function deleteCategorie(ctx) {
  const { id } = ctx.params

  try {
    await deleteCat(id)
  } catch (e) {
    ctx.status = 500
    console.log("Error pending delete categorie", e)
    return
  }

  ctx.status = 200
}
