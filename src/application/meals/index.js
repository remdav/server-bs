import {
    allMeals,
    allCat,
    createCat,
    updateCat,
    deleteCategorie,
    createMeal,
    updateMeal,
    deleteMeal,
    oneMeals,
    mealsByCat
} from "./action"
import Router from "koa-router"

const route = new Router()

route.get("/categories", allCat)
route.get("/mealsbycat/:id", mealsByCat)
route.post("/categories", createCat)
route.put("/categories/:id", updateCat)
route.delete("/categories/:id", deleteCategorie)
route.get("/:id", oneMeals)
route.get("/", allMeals)
route.post("/", createMeal)
route.put("/:id", updateMeal)
route.delete("/:id", deleteMeal)

export default route