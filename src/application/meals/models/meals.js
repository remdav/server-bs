import { DataTypes } from "sequelize"
import app from "server"

const Meals = app.context.sequelize.define('meals', {
  title: {
    type: DataTypes.STRING(50),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  describe: {
    type: DataTypes.TEXT,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  stock: {
    type: DataTypes.INTEGER(5),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  price: {
    type: DataTypes.INTEGER(5),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  picture: {
    type: DataTypes.STRING(100),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  categorieId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: "categories",
      key: "id"
    }
  }
}, {
  indexes: [
    {
      unique: true,
      fields: ["title"]
    }
  ]
})

Meals.associate = function(models) {
  Meals.belongTo(models.Categories, { foreignKey: "categories_id", as: "categories" })
}

export default Meals

