import { DataTypes } from "sequelize"
import app from "server"

const Categories = app.context.sequelize.define('categories', {
  title: {
    type: DataTypes.STRING(50),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  picture: {
    type: DataTypes.STRING(100),
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
}, {
  indexes: [
    {
      unique: true,
      fields: ["title"]
    }
  ]
})

Categories.associate = function(models) {
  Categories.hasMany(models.Meals, { as: "meals" })
}

export default Categories