import { Op } from "sequelize"

import { meals, categories } from "./models"

export async function getAllMeals(title) {

  return meals.findAll({ where: { title: { [Op.like]: `%${title}%` }  } })
}

export async function AllMealsByCategories(categorieId, title) {
  return meals.findAll({
    where: {
      [Op.and]: [
        { categorieId },
        { title: { [Op.like]: `%${title}%` } }
      ]
    }
  })
}

export async function getMeal(id) {
  return meals.findByPk(id)
}

export async function createNewMeal(meal) {
  let newMeal

  try {
    newMeal = await meals.build(meal).save()
  } catch (e) {
    console.log("Error create new meal in db", e)
    return
  }

  return newMeal.dataValues
}

export async function updateOneMeal(id, update) {
  try {
    await meals.update(update, { where: { id } })
  } catch (e) {
    console.log("Error pending update meal in db", e)
    return
  }

  return meals.findByPk(id)
}

export async function deleteOneMeal(id) {
  return await meals.destroy({ where: { id } })
}

export function allCategories() {
  return categories.findAll()
}

export async function createcategorie(title, picture) {
  let newcategorie

  try {
    newcategorie = await categories.create({ title, picture })
  } catch (e) {
    console.log("Error pending creation new categorie meal", e)
    return
  }

  return newcategorie
}

export async function updateCategorie(id, title, picture) {
  try {
    await categories.update({ title, picture }, { where: { id } })
  } catch (e) {
    console.log("Error pending updating categorie", e)
    return
  }

  return await categories.findByPk(id)
}

export async function deleteCat(id) {
  return await categories.destroy({ where: { id } })
}
