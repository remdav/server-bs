import { configureJwt, keyRandom } from "utils"

import { register } from "./service"

export async function create(ctx){
    const { username, name, password, firstname, type, addressMail, zipCode, address, phoneNumber } =  ctx.request.body
    let create

    try {
        create = await register(username, name, password, firstname, type, addressMail, zipCode, address, phoneNumber)
    } catch (e) {
        console.log("Error pending create new user", e)
        ctx.status = 500
        return
    }

    if(!!create) {
        const key = keyRandom()

        return ctx.body = {
            items: {
                tokenKey: await configureJwt({name: name, role: create.dataValues.role}, key),
                key: key
            }
        }       
    }

    ctx.body = { text: "USER_EXIST" }
}
