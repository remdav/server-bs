import { DataTypes } from "sequelize"
import app from "server"

const Users = app.context.sequelize.define('users', {
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  firstname: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  addressMail: {
    type: DataTypes.STRING,
    allowNull: false,
    isUnique: true,
    validate: {
      isEmail: {
        msg: "Merci de saisir un email correct"
      },
      notEmpty: {
        msg: "* champ obligatoire"
      },
    }
  },
  phoneNumber: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  address: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  zipCode: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  role: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: "user",
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  }
}, {
  indexes:[
   {
     unique: false,
     fields:["firstname"]
   }, {
     unique: false,
     fields: ["name"]
   }
  ]
})

Users.relation = function(models) {
  Users.hasMany(models.Animations, { as: "animations" })
}

export default Users