import { DataTypes } from "sequelize"
import app from "server"

const externalUsers = app.context.sequelize.define('externalUsers', {
  idExternal: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  },
  role: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: "user",
    validate: {
      notEmpty: {
        msg: "* champ obligatoire"
      }
    }
  }
})

export default externalUsers