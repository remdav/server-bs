import bcrypt from "bcrypt"

import { users, externalUser } from "./models"

export async function register(username, name, password, firstname, type = "customer", addressMail, zipCode, address, phoneNumber) {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(password, salt)
    const isUserExist = await users.findOne({ where: {
        username: username,
    } })


    if (await isUserExist) {
        return
    }
    let newUser

    try {
        newUser = await users.build({ username, name, password: hash, firstname, type, addressMail, zipCode, address, phoneNumber, role: "user" }).save()
    } catch (e) {
        console.log("Error pending create new user", e)
        return
    }

    return newUser
}

export async function createExternalUser(info) {
    let newExternal

    try {
        newExternal = await externalUser.build(info).save()
    } catch(e) {
        console.log("Error pending create new external user", e)
        return
    }

    return newExternal
}