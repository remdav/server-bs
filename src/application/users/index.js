import { create } from "./action"
import Router from "koa-router"

const route = new Router()

route.post("/", create)

export default route