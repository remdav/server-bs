import randomWord from "random-words"
import md5 from "md5"

export default function keyRandom() {
  return md5(randomWord({ min: 30, max: 100 }))
}