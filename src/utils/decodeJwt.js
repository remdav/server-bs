import config from "configs/server.json"
import jwt from "jsonwebtoken"

export default function configureJwt(token) {
  return jwt.verify(token, config.global.jwt.secureKey)
}