import config from "configs/server.json"
import jwt from "jsonwebtoken"

export default function configureJwt(info) {
  const accesJwt = jwt.sign({
    info
  },
    config.global.jwt.secureKey,
    {
      expiresIn: Date.now() + (1000 * 60 * 60 * 24),
    }
  )

  return accesJwt
}